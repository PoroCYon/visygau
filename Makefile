
default: debug

%/:
	@mkdir -p $@

LIBS=-lm -lGLEW -lGL -lglfw
CFLAGS=-Iinc -std=gnu11
ASFLAGS=-Iinc

AS=nasm
CC_ ?= clang
CC:=$(CC_)

ifneq (,$(findstring clang,$(CC)))
	CFLAGS+=-Weverything -Wpedantic -Wno-language-extension-token\
        -Wno-flexible-array-extensions -Wno-vla \
        -Wno-gnu-statement-expression
else
	CFLAGS+=-Wall
endif

obj/%.o: src/%.asm obj/
	$(AS) $(ASFLAGS) -f elf64 -o "$@" "$<"

obj/%.o: src/%.c obj/
	$(CC) $(CFLAGS) -o "$@" -c "$<"

bin/visygau: obj/shaders.o obj/fbdev.o obj/glfb.o obj/visygau.o
	$(CC) $(CFLAGS) -o "$@" $^ $(LIBS)
bin/nurxru: obj/fbdev.o obj/nurxru.o
	$(CC) $(CFLAGS) -o "$@" $^ $(LIBS)
bin/bregau: obj/bregau.o
	$(CC) $(CFLAGS) -o "$@" $^ $(LIBS)

bin/vfb.ko:
	$(MAKE) -f mkvfb.mk "$@"

all: bin/ bin/nurxru bin/visygau bin/bregau bin/vfb.ko
	echo $(CFLAGS) | sed 's/ /\n/g' > .clang_complete
	< .clang_complete sed 's/inc/\.\.\/inc/g' > src/.clang_complete

debug: CFLAGS += -DDEBUG -g -O1
debug: ASFLAGS += -DDEBUG -g -O0
debug: all

release: CFLAGS += -DNODEBUG -DNDEBUG -DRELEASE -O3
release: ASFLAGS += -DNODEBUG -DNDEBUG -DRELEASE
release: all

clean:
	@-rm -rvf bin/* obj/*

mkvfb.mk:
	$(MAKE) -f "$@"

.PHONY: all debug release clean default mkvfb.mk

