
#ifndef VG_GLFB_H_
#define VG_GLFB_H_

#include <GLFW/glfw3.h>

#include "fbdev.h"

// -> draw:
// * glTexImage2D
// * frag shader: clip, scale etc.
// * actually draw

struct vg_glstate;

struct vg_glstate* vg_mkglstate(struct vg_fbdev* fbdev);
void vg_killglstate(struct vg_glstate* gl);

void vg_glupdate(struct vg_glstate* gl);
void vg_glrender(struct vg_glstate* gl);

void vg_glmainloop(struct vg_glstate* gl, GLFWwindow* wnd);

#endif

