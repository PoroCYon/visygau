
#ifndef VG_FBDEV_H_
#define VG_FBDEV_H_

#include <stddef.h>
#include <stdint.h>
#include <stdbool.h>

#include <linux/fb.h>

struct vg_fbdev
{
    void* mem;
    size_t mapsz;
    int filed;
    bool ro;
    bool tty;
    uint16_t _pad;

    struct fb_fix_screeninfo fixinf;
    struct fb_var_screeninfo varinf;
};

struct vg_fbdev* vg_fbdev_init(const char* file,
        bool readonly, // does what you think it does
        bool istty     // fb will receive an extra ioctl(_, KDSETMODE,
                       // KD_GRAPHICS) (will be reset automatically on
                       // kill), requires readonly to be false
);
void vg_fbdev_kill(struct vg_fbdev* fbdev);

bool vg_fbdev_update(struct vg_fbdev* fbdev);
bool vg_fbdev_apply (struct vg_fbdev* fbdev);

void vg_RED_ALERT_RETURN_TO_TEXTMODE(void);

#endif

