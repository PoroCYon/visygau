#!/usr/bin/env bash

FB="$1"
if [ -z "$FB" ]; then
    FB=/dev/fb1
fi

OUF="$2"
if [ -z "$OUF" ]; then
    OUF="$(date '+%F-%H-%M-%S-fbdev-grab.png')"
fi

ffmpeg -f fbdev -i "$FB" -frames 1 "$OUF"
