#!/usr/bin/env bash

OUF="$1"
if [ -z "$OUF" ]; then
    OUF="$(date '+%F-%H-%M-%S-fbdev-capture.mp4')"
fi

FB="$2"
if [ -z "$FB" ]; then
    FB=/dev/fb1
fi

FPS="$3"
if [ -z "$FPS" ]; then
    FPS=24
fi

ffmpeg -f fbdev -framerate "$FPS" -i "$FB" -r "$FPS" -filter_complex '[rgb_in][alpha_in]; ... [rgb_out]; ... [alpha_out]' -map '[rgb_out]' "$OUF"

