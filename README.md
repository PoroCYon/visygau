# visygau

A collection of fbdev-related tools.

## `bin/visygau`

Display the content of the specififed framebuffer device. Deprecated in favor
of `fbdev-view.sh`, unless the latter breaks. (It does with transparency...)

## `bin/bregau`

When a `vfb` is created, its color mode is bogus and the `line_length` is 0.
This program uses a workaround to correct the latter. See
[`src/bregau.c`](src/bregau.c) for a more detailed description.

Deprecated since my patch was accepted. If your kernel doesn't come with a
`vfb` module, use `mkvfb.mk`.

## `bin/fbq`

Query fbdev info. Deprecated in favor of `fbset -i -fb /dev/fb<n>`.

## `bin/nurxru`

Make `/dev/tty` return to textmode. Useful if a program that sets it to
graphical mode crashes without returning to textmode.

## Shell scripts

### `mkvfb.mk`

(Actually a makefile)

Load (`insmod`) the `vfb` kernel module, including my fix, compile if
necessary.

Greets to Mr. BitBlit of Team-X Belgium for helping me fix the module.

### `fbdev-capture.sh`

Use FFmpeg to capture fbdev content as a video file.

### `fbdev-grab.sh`

Take a screenshot of an fbdev. Uses FFmpeg.

### `fbdev-view.sh`

Shows the content of the specified fbdev using FFplay.

## License

[SAL](/LICENSE)

### NOTE

This repository contains one file originating from the Linux kernel, but isn't
directly used by or linked to from the other code, so that single file remains
under the GPLv2.

