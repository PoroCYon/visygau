
#include <stddef.h>
#include <stdint.h>
#include <stdbool.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>

#include <unistd.h>
#include <sys/types.h>
#include <sys/ioctl.h>
#include <sys/kd.h>
#include <sys/mman.h>
#include <sys/stat.h>
#include <fcntl.h>

#include <linux/fb.h>

static void print_usage(const char* progname) {
    dprintf(STDERR_FILENO,
"Usage: %s <file>\n"
"\tDump the contents of the fb_fix_screeninfo and \n"
"\tfb_var_screeninfo structs of the specified fbdev.\n"
"This tool has been deprecated in favor of `fbset -i -fb /dev/fbn'\n",
        progname);
}

int main(int argc, char* argv[]) {
    if (argc < 2 || !strcmp("--help",argv[0]) || !strcmp("-h",argv[0])) {
        print_usage(argv[0]);
        return 1;
    }

    int filed = open(argv[1], O_RDONLY);
    if (filed < 0) {
        dprintf(STDERR_FILENO, "open(\"%s\") failed: %i\n", argv[1], -filed);
        return 1;
    }

    struct fb_fix_screeninfo fix;
    struct fb_var_screeninfo var;

    if (ioctl(filed, FBIOGET_FSCREENINFO, &fix))
        dprintf(STDERR_FILENO, "ioctl for FBIOGET_FSCREENINFO failed!\n");
    else
        printf("fix = { id = \"%s\", smem_len = 0x%X, type = %u, type_aux = %u,\n"
                "       visual = %u, xpanstep = %u, ypanstep = %u, ywrapstep = %u,\n"
                "       line_length = %u, accel = %u, capabilities = %u }\n",
                fix.id, fix.smem_len, fix.type, fix.type_aux,
                fix.visual, fix.xpanstep, fix.ypanstep, fix.ywrapstep,
                fix.line_length, fix.accel, fix.capabilities);

    if (ioctl(filed, FBIOGET_VSCREENINFO, &var))
        dprintf(STDERR_FILENO, "ioctl for FBIOGET_VSCREENINFO failed!\n");
    else
        printf("var = { xres = %u, yres = %u, xres_virtual = %u, yres_virtual = %u,\n"
               "        xoffset = %u, yoffset = %u, bits_per_pixel = %u, grayscale = %u,\n"
               "        red = %u:%u, green = %u:%u, blue = %u:%u, transp = %u:%u,\n"
               "        nonstd = %u, activate = %u, height = %u, width = %u,\n"
               "        accel_flags = %u }\n",
               var.xres, var.yres, var.xres_virtual, var.yres_virtual,
               var.xoffset, var.yoffset, var.bits_per_pixel, var.grayscale,
               var.red.offset,var.red.length, var.green.offset,var.green.length,
               var.blue.offset,var.green.length, var.transp.offset,var.transp.length,
               var.nonstd, var.activate, var.height, var.width); // cba to print timing stuff

    close(filed);

    return 0;
}

