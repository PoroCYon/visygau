
#include <stddef.h>
#include <stdint.h>
#include <stdbool.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>

#include <unistd.h>
#include <sys/types.h>
#include <sys/ioctl.h>
#include <sys/kd.h>
#include <sys/mman.h>
#include <sys/stat.h>
#include <fcntl.h>

#include <linux/fb.h>

/*
 * There's a bug in the `vfb' module, introduced in 2002 during an
 * architectural rewirte, causing the video mode not to be set when the
 * module is loaded, leaving the line_length to 0. This can be circumvented
 * by calling the FBIOPUT_VSCREENINFO ioctl on the newly created fbdev with
 * its `activate` flags set to FORCE | ALL | NOW. The color mode, however,
 * cannot be 'fixed' by a workaround. This is what this utility does.
 *
 * I have submitted a patch, but at the time of writing, it is not yet
 * accepted. See https://lkml.org/lkml/2017/12/20/409 for more information
 * on the patch.
 */

static void print_usage(const char* progname) {
    dprintf(STDERR_FILENO,
"Usage: %s <file> <fb to clone>?\n"
"\tEnsure the framebuffer has a valid line length after loading\n"
"\tthe vfb kernel module. Optionally, clone settings from another\n"
"\tframebuffer device.\n",
        progname);
}

int main(int argc, char* argv[]) {
    if (argc < 2 || !strcmp("--help",argv[0]) || !strcmp("-h",argv[0])) {
        print_usage(argv[0]);
        return 1;
    }

    const char* clonefrom = argc == 2 ? argv[1] : argv[2];

    int filed = open(clonefrom, argc == 2 ? O_RDWR : O_RDONLY);
    if (filed < 0) {
        dprintf(STDERR_FILENO, "open(\"%s\") failed: %i\n", clonefrom, -filed);
        return 1;
    }

    struct fb_var_screeninfo var;

    ioctl(filed, FBIOGET_VSCREENINFO, &var);
    var.activate |= FB_ACTIVATE_FORCE | FB_ACTIVATE_ALL | FB_ACTIVATE_NOW; // FB_ACTIVATE_INV_MODE

    if (argc > 2) {
        close(filed);

        filed = open(argv[1], O_RDWR);
        if (filed < 0) {
            dprintf(STDERR_FILENO, "open(\"%s\") failed: %i\n", argv[1], -filed);
            return 1;
        }
    }

    ioctl(filed, FBIOPUT_VSCREENINFO, &var);

    close(filed);

    return 0;
}

