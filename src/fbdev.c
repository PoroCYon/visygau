
#include <stddef.h>
#include <stdint.h>
#include <stdbool.h>
#include <stdlib.h>
#include <string.h>

#include <unistd.h>
#include <sys/types.h>
#include <sys/ioctl.h>
#include <sys/kd.h>
#include <sys/mman.h>
#include <sys/stat.h>
#include <fcntl.h>

#include <linux/fb.h>

#include "fbdev.h"

struct vg_fbdev* vg_fbdev_init(const char* file, bool ro, bool tty) {
    int filed = open(file, ro ? O_RDONLY : O_RDWR);

    if (filed < 0) {
        return NULL;
    }

    struct fb_fix_screeninfo finf;
    struct fb_var_screeninfo vinf;

    if (ioctl(filed, FBIOGET_FSCREENINFO, &finf) < 0) {
        return NULL;
    }
    if (ioctl(filed, FBIOGET_VSCREENINFO, &vinf) < 0) {
        return NULL;
    }

    size_t memsz = (size_t)vinf.yres_virtual * finf.line_length;
    void* mem = mmap(NULL, memsz, ro?PROT_READ:PROT_READ|PROT_WRITE, MAP_SHARED, filed, 0);
    if (!mem || mem == MAP_FAILED) {
        return NULL;
    }

    struct vg_fbdev* r = (struct vg_fbdev*)calloc(sizeof(struct vg_fbdev), 1);
    if (!r) {
        return NULL;
    }

    r->mem   = mem  ;
    r->mapsz = memsz;
    r->filed = filed;

    if (!(r->ro = ro)) {
        r->tty = tty && ioctl(filed, KDSETMODE, KD_GRAPHICS) >= 0;
    }

    memcpy(&r->fixinf, &finf, sizeof(struct fb_fix_screeninfo));
    memcpy(&r->varinf, &vinf, sizeof(struct fb_var_screeninfo));

    return r;
}

void vg_fbdev_kill(struct vg_fbdev* fb) {
    if (!fb) {
        return;
    }

    if (fb->mem && fb->mapsz) {
        munmap(fb->mem, fb->mapsz);
    }
    if (fb->filed) {
        if (fb->tty) {
            ioctl(fb->filed, KDSETMODE, KD_TEXT);
        }

        close(fb->filed);
    }

    fb->filed =
        (int  )(fb->mem =
        (void*)(fb->mapsz ^= fb->mapsz));

    free(fb);
}

bool vg_fbdev_update(struct vg_fbdev* fb) {
    if (!fb || !fb->filed) {
        return false;
    }

    return ioctl(fb->filed, FBIOGET_VSCREENINFO, &fb->varinf) >= 0;
}

bool vg_fbdev_apply(struct vg_fbdev* fb) {
    if (!fb || !fb->filed || fb->ro) {
        return false;
    }

    if (!(ioctl(fb->filed, FBIOPUT_VSCREENINFO, &fb->varinf) >= 0 // '&&' - don't try another ioctl if the first one failed
       && ioctl(fb->filed, FBIOGET_VSCREENINFO, &fb->varinf) >= 0)) {
        return false;
    }

    // now we need to remap the memory if the video size changed
    size_t nmapsz = (size_t)fb->varinf.yres_virtual * fb->fixinf.line_length;
    if (fb->mapsz == nmapsz) {
        return true;
    }

    if (fb->mem && fb->mapsz) {
        munmap(fb->mem, fb->mapsz);

        fb->mem = (void*)(fb->mapsz ^= fb->mapsz);
    }

    void* mem = mmap(NULL, nmapsz, fb->ro?PROT_READ:PROT_READ|PROT_WRITE, MAP_SHARED, fb->filed, 0);
    if (!mem || mem == MAP_FAILED) {
        return false; // oh shit
    }

    fb->mem   = mem   ;
    fb->mapsz = nmapsz;

    return true;
}

void vg_RED_ALERT_RETURN_TO_TEXTMODE() {
    int filed = open("/dev/tty0", O_RDWR);
    if (filed < 0) {
        return;
    }

    ioctl(filed, KDSETMODE, KD_TEXT);
    close(filed);
}

