bits 64

section .rodata

global fbviewer_frag
global fbviewer_vert

fbviewer_frag:
    incbin "shdr/frag.glsl"
    db 0

fbviewer_vert:
    incbin "shdr/vert.glsl"
    db 0
