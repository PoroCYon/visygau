
#include <stdio.h>
#include <unistd.h>
#include <string.h>

#include "fbdev.h"

static inline void print_usage(const char* this) {
    dprintf(STDERR_FILENO,
"Usage: %s\n"
"\tMakes /dev/tty0 return to text mode.\n",
        this);
}

int main(int argc, char* argv[]) {
    if (argc < 2 || !strcmp("--help",argv[0]) || !strcmp("-h",argv[0])) {
        print_usage(argv[0]);
        return 1;
    }

    // requires the user to be in the 'video' group
    vg_RED_ALERT_RETURN_TO_TEXTMODE();
    return 0;
}

