
#include <stddef.h>
#include <stdint.h>
#include <stdbool.h>
#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>
#include <string.h>

#include <GL/glew.h>
#include <GLFW/glfw3.h>

#include "fbdev.h"
#include "glfb.h"

extern const char fbviewer_frag[],
                  fbviewer_vert[];

struct vg_glstate
{
    struct vg_fbdev* fb;
    GLuint tex ;     // gltexture containing the framebuffer data
    GLuint prgm;     // shader
    GLuint va;       // vertex array
    GLuint vb;       // vertex buffer
    // stuff will probably break if there's padding here
    GLuint pb;       // pixel buffer
    GLint  unifs[2]; // uniform indices
    GLint  attrs[2]; // attribute indices
};

static const float verts[5 * 4] = {
    -1, -1, 0, 0, 1, // use texcoords that don't suck
    -1,  1, 0, 0, 0,
     1, -1, 0, 1, 1,
     1,  1, 0, 1, 0
};

static int neww, newh;

#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Wunused-parameter"
static void glfwsz_cb(GLFWwindow* wnd, int nw, int nh) {
#pragma clang diagnostic pop
    neww = nw;
    newh = nh;
}

static bool check_shdr(GLuint shdr) {
    GLint succ;

    glGetShaderiv(shdr, GL_COMPILE_STATUS, &succ);
    if (!succ) {
        GLint len;
        glGetShaderiv(shdr, GL_INFO_LOG_LENGTH, &len);
        char logs[len + 1];
        glGetShaderInfoLog(shdr, len, &len, logs);

        dprintf(STDERR_FILENO, "shader compilation failed:\n%s\n", logs);

        return false;
    }

    return true;
}
static bool check_prgm(GLuint prgm) {
    GLint succ;

    glGetProgramiv(prgm, GL_LINK_STATUS, &succ);
    if (!succ) {
        GLint len;
        glGetProgramiv(prgm, GL_INFO_LOG_LENGTH, &len);
        char logs[len + 1];
        glGetProgramInfoLog(prgm, len, &len, logs);

        dprintf(STDERR_FILENO, "program linking failed:\n%s\n", logs);

        return false;
    }

    return true;
}
static GLuint mkshader(const char* fs, const char* vs) {
    GLuint fso = glCreateShader(GL_FRAGMENT_SHADER),
           vso = glCreateShader(GL_VERTEX_SHADER  );

    glShaderSource(fso, 1, &fs, 0);
    glShaderSource(vso, 1, &vs, 0);

    glCompileShader(fso);
    glCompileShader(vso);

    if (!check_shdr(fso) || !check_shdr(vso)) {
        glDeleteShader(fso);
        glDeleteShader(vso);

        return ~(GLuint)0;
    }

    GLuint prgm = glCreateProgram();
    glAttachShader(prgm, fso);
    glAttachShader(prgm, vso);
    glLinkProgram(prgm);

    glDeleteShader(fso);
    glDeleteShader(vso);

    if (!check_prgm(prgm)) {
        glDeleteProgram(prgm);

        return ~(GLuint)0;
    }

    return prgm;
}

struct vg_glstate* vg_mkglstate(struct vg_fbdev* fb) {
    // only 32-bit true colour RGBA supported for now
    if (fb->varinf.bits_per_pixel != 32) {
        return NULL;
    }

    GLuint prgm = mkshader(fbviewer_frag, fbviewer_vert);
    if (!~prgm) {
        return NULL;
    }

    struct vg_glstate* r = (struct vg_glstate*)
        calloc(sizeof(struct vg_glstate), 1);

    r->fb   = fb  ;
    r->prgm = prgm;

    glGenVertexArrays(1, &r->va );
    glGenTextures    (1, &r->tex);
    glGenBuffers     (2, &r->vb );

    glBindBuffer(GL_ARRAY_BUFFER, r->vb);
    glBufferData(GL_ARRAY_BUFFER, sizeof(verts), verts, GL_STATIC_DRAW);
    glBindBuffer(GL_ARRAY_BUFFER, 0);

    int u = (int)fb->varinf.xres_virtual,
        v = (int)fb->varinf.yres_virtual;

    glBindBuffer(GL_PIXEL_UNPACK_BUFFER, r->pb);
    glBufferData(GL_PIXEL_UNPACK_BUFFER, u * v *
        (int)(fb->varinf.bits_per_pixel >> 3),
        NULL/*fb->mem*/, GL_STREAM_DRAW);
    glBindBuffer(GL_PIXEL_UNPACK_BUFFER, 0);

    glBindTexture(GL_TEXTURE_2D, r->tex);
    //glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, u, v, 0, GL_BGRA,
    //        GL_UNSIGNED_BYTE, fb->mem);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
    glBindTexture(GL_TEXTURE_2D, 0);

    r->unifs[0] = glGetUniformLocation(prgm, "res");
    r->unifs[1] = glGetUniformLocation(prgm, "off");

    r->attrs[0] = glGetAttribLocation(prgm, "ipos");
    r->attrs[1] = glGetAttribLocation(prgm, "itc" );

    return r;
}

void vg_killglstate(struct vg_glstate* gl) {
    if (!gl) {
        return;
    }

    glDeleteTextures(1, &gl->tex);
    glDeleteProgram(gl->prgm);
    glDeleteVertexArrays(1, &gl->va);
    glDeleteBuffers(2, &gl->vb);

    free(gl);
}

void vg_glupdate(struct vg_glstate* gl) {
    vg_fbdev_update(gl->fb);

    int w = (int)gl->fb->varinf.xres   ,
        h = (int)gl->fb->varinf.yres   ,
        u = (int)gl->fb->varinf.xres_virtual,
        v = (int)gl->fb->varinf.yres_virtual,
        x = (int)gl->fb->varinf.xoffset,
        y = (int)gl->fb->varinf.yoffset;

    glUseProgram(gl->prgm);

    glUniform4f(gl->unifs[0], w, h, u, v);
    glUniform2f(gl->unifs[1], x, y);

    glUseProgram(0);
}
void vg_glrender(struct vg_glstate* gl) {
    int u = (int)(gl->fb->fixinf.line_length / (gl->fb->varinf.bits_per_pixel >> 3)),
        v = (int)(gl->fb->mapsz              / gl->fb->fixinf.line_length);

    glActiveTexture(GL_TEXTURE0);
    glBindTexture(GL_TEXTURE_2D, gl->tex);
    glBindBuffer(GL_PIXEL_UNPACK_BUFFER, gl->pb);
    //glBufferData(GL_PIXEL_UNPACK_BUFFER, u * v *
    //    (int)(gl->fb->varinf.bits_per_pixel >> 3),
    //    NULL/*gl->fb->mem*/, GL_STREAM_DRAW);
    void* ptr = glMapBuffer(GL_PIXEL_UNPACK_BUFFER, GL_WRITE_ONLY);
    if (ptr) {
        memcpy(ptr, gl->fb->mem, gl->fb->mapsz);
        glUnmapBuffer(GL_PIXEL_UNPACK_BUFFER);
    }
    glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, u, v, 0, GL_BGRA,
            GL_UNSIGNED_BYTE, 0);//gl->fb->mem);
    glBindBuffer(GL_PIXEL_UNPACK_BUFFER, 0);

    glBindVertexArray(gl->va);
    glUseProgram(gl->prgm);
    glBindBuffer(GL_ARRAY_BUFFER, gl->vb);

    glVertexAttribPointer(gl->attrs[0], 3, GL_FLOAT, GL_FALSE,
        sizeof(float) * 5, (GLvoid*)(sizeof(float) * 0));
    glVertexAttribPointer(gl->attrs[1], 2, GL_FLOAT, GL_FALSE,
        sizeof(float) * 5, (GLvoid*)(sizeof(float) * 3));

    glEnableVertexAttribArray(gl->attrs[0]);
    glEnableVertexAttribArray(gl->attrs[1]);

    glDrawArrays(GL_TRIANGLE_STRIP, 0, 4);

    glDisableVertexAttribArray(gl->attrs[0]);
    glDisableVertexAttribArray(gl->attrs[1]);

    glBindBuffer(GL_ARRAY_BUFFER, 0);
    glUseProgram(0);
    glBindVertexArray(0);

    glBindTexture(GL_TEXTURE_2D, 0);
}

void vg_glmainloop(struct vg_glstate* gl, GLFWwindow* wnd) {
    int w = (int)gl->fb->varinf.xres,
        h = (int)gl->fb->varinf.yres;

    glfwSetFramebufferSizeCallback(wnd, glfwsz_cb);

    glfwSwapInterval(0);

    glClearColor(0, 0, 0, 1);

    glViewport(0, 0, neww = w, newh = h);
    glClear(GL_COLOR_BUFFER_BIT);

    do {
        glfwPollEvents();

        if (glfwGetKey(wnd, GLFW_KEY_ESCAPE) == GLFW_PRESS)
            glfwSetWindowShouldClose(wnd, GL_TRUE);

        vg_glupdate(gl);

        if (neww != w || newh != h) {
            glViewport(0, 0, w = neww, h = newh);
        }

        glClear(GL_COLOR_BUFFER_BIT);
        vg_glrender(gl);

        glfwSwapBuffers(wnd);
    } while (!glfwWindowShouldClose(wnd));
}

