
#include <stdio.h>
#include <unistd.h>
#include <string.h>

#include <GL/glew.h>
#include <GLFW/glfw3.h>

#include "fbdev.h"
#include "glfb.h"

static inline void print_usage(const char* this) {
    dprintf(STDERR_FILENO,
"Usage: %s <file>\n"
"\tDisplays the content of the given framebuffer in a GLFW window.\n"
"Please use fbdev-view.sh unless that one doesn't work.\n",
        this);
}

#ifndef NDEBUG
static void glfwerr_cb(int code, const char* msg) {
    dprintf(STDERR_FILENO, "GLFW error (%i, 0x%X): %s\n",
            code, code, msg);
}

#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Wunused-parameter"
static void APIENTRY gldbg_cb(GLenum src, GLenum type, GLuint id,
        GLenum sev, GLsizei len, const GLchar* msg, const void* ud) {
#pragma clang diagnostic pop
    if (sev != GL_DEBUG_SEVERITY_HIGH_ARB && sev != GL_DEBUG_SEVERITY_MEDIUM_ARB
            && sev != GL_DEBUG_SEVERITY_LOW_ARB) {
        return;
    }

    dprintf(STDERR_FILENO, "GL debug ");
    switch (src) {
        case GL_DEBUG_SOURCE_API_ARB: dprintf(STDERR_FILENO, "api "); break;
        case GL_DEBUG_SOURCE_WINDOW_SYSTEM_ARB: dprintf(STDERR_FILENO, "wnd sys "); break;
        case GL_DEBUG_SOURCE_SHADER_COMPILER_ARB: dprintf(STDERR_FILENO, "glslc "); break;
        case GL_DEBUG_SOURCE_THIRD_PARTY_ARB: dprintf(STDERR_FILENO, "3rd "); break;
        case GL_DEBUG_SOURCE_APPLICATION_ARB: dprintf(STDERR_FILENO, "appl "); break;
        case GL_DEBUG_SOURCE_OTHER_ARB: dprintf(STDERR_FILENO, "other-s "); break;
    }

    switch (type) {
        case GL_DEBUG_TYPE_ERROR_ARB: dprintf(STDERR_FILENO, "err "); break;
        case GL_DEBUG_TYPE_DEPRECATED_BEHAVIOR_ARB: dprintf(STDERR_FILENO, "depr "); break;
        case GL_DEBUG_TYPE_UNDEFINED_BEHAVIOR_ARB: dprintf(STDERR_FILENO, "undef "); break;
        case GL_DEBUG_TYPE_PORTABILITY_ARB: dprintf(STDERR_FILENO, "port "); break;
        case GL_DEBUG_TYPE_PERFORMANCE_ARB: dprintf(STDERR_FILENO, "perf "); break;
        case GL_DEBUG_TYPE_OTHER_ARB: dprintf(STDERR_FILENO, "other-t "); break;
    }

    switch (sev) {
        case GL_DEBUG_SEVERITY_HIGH_ARB: dprintf(STDERR_FILENO, "high"); break;
        case GL_DEBUG_SEVERITY_MEDIUM_ARB: dprintf(STDERR_FILENO, "med"); break;
        case GL_DEBUG_SEVERITY_LOW_ARB: dprintf(STDERR_FILENO, "low"); break;
        default: dprintf(STDERR_FILENO, "note"); break;
        //case GL_DEBUG_SEVERITY_NOTIFICATION_ARB: dprintf(STDERR_FILENO, "note "); break;
    }

    dprintf(STDERR_FILENO, ": %i (0x%X): %s\n", id, id, msg);

    if (type == GL_DEBUG_TYPE_ERROR_ARB || sev == GL_DEBUG_SEVERITY_HIGH_ARB) {
        asm("int3");
    }
}
#endif

#define RET(x) do{retv=(x);goto END;}while(0)
int main(int argc, char* argv[]) {
    if (argc < 2 || !strcmp("--help",argv[0]) || !strcmp("-h",argv[0])) {
        print_usage(argv[0]);
        return 1;
    }

    struct vg_fbdev* fb = NULL;
    GLFWwindow* wnd = NULL;
    struct vg_glstate* gl = NULL;
    int retv = 0;

#ifndef NDEBUG
    glfwSetErrorCallback(glfwerr_cb);
#endif

    glfwInit();

    glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 4);
    glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 3);
    glfwWindowHint(GLFW_RED_BITS  , 8);
    glfwWindowHint(GLFW_GREEN_BITS, 8);
    glfwWindowHint(GLFW_BLUE_BITS , 8);
    glfwWindowHint(GLFW_ALPHA_BITS, 8);
    glfwWindowHint(GLFW_DOUBLEBUFFER, GL_TRUE);
    glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);
#ifndef NDEBUG
    glfwWindowHint(GLFW_OPENGL_DEBUG_CONTEXT, GL_TRUE);
#endif

    if (!(fb = vg_fbdev_init(argv[1], true, false))) {
        dprintf(STDERR_FILENO, "Framebuffer initialisation failed.\n");
        RET(1);
    }

    int w = (int)fb->varinf.xres,
        h = (int)fb->varinf.yres;

    if (!(wnd = glfwCreateWindow(w, h, "fbviewer", NULL, NULL))) {
        dprintf(STDERR_FILENO, "GLFW initialisation failed.\n");
        RET(1);
    }

    glfwMakeContextCurrent(wnd);

    glewExperimental = GL_TRUE;
    GLenum err = glewInit();
    glGetError();
    if (err != GLEW_OK) {
        dprintf(STDERR_FILENO, "GLEW initialisation failed.\n");
        RET(1);
    }

#ifndef NDEBUG
    GLint flags;
    glGetIntegerv(GL_CONTEXT_FLAGS, &flags);
    if (flags & GL_CONTEXT_FLAG_DEBUG_BIT) {
        glEnable(GL_DEBUG_OUTPUT);
        glEnable(GL_DEBUG_OUTPUT_SYNCHRONOUS);
        glDebugMessageCallback(gldbg_cb, NULL);
        glDebugMessageControl(GL_DONT_CARE, GL_DONT_CARE, GL_DONT_CARE,
                0, NULL, GL_TRUE);
    }
#endif

    if (!(gl = vg_mkglstate(fb))) {
        dprintf(STDERR_FILENO, "OpenGL initialisation failed.\n");
        RET(1);
    }

    vg_glmainloop(gl, wnd);

END:
    if (gl) {
        vg_killglstate(gl);
    }
    if (wnd) {
        glfwDestroyWindow(wnd);
    }
    if (fb) {
        vg_fbdev_kill(fb);
    }
    glfwTerminate();

    return retv;
}

