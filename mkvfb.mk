#!/usr/bin/make -f

MODE_OPTION ?= 1366x768-32@60
VIDEOMEM    ?= 4196352

default: bin/vfb.ko
	sudo insmod "$<" "mode_option=$(MODE_OPTION)" "videomemorysize=$(VIDEOMEM)" vfb_enable=1

bin/vfb.ko: src/vfb/vfb.c
	$(MAKE) -C src/vfb/ OUTFILE="$(shell pwd)/$@"

clean:
	$(MAKE) -C src/vfb/ clean

