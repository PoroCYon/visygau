#!/usr/bin/env bash

FB="$1"
if [ -z "$FB" ]; then
    FB=/dev/fb1
fi

FPS="$2"
if [ -z "$FPS" ]; then
    FPS=24
fi

ffplay -f fbdev -framerate "$FPS" -i "$FB"

