#version 430 core

uniform vec4 res; // xres, yres, xres_virtual, yres_virtual
uniform vec2 off;

uniform sampler2D fb;

in vec2 otc;

layout (location = 0) out vec4 col;

void main() {
    //vec2 start = off,
    //     end   = (res.xy + off) / res.zw;

    col = texture(fb, otc);
}

